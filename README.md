# Anthos Config Management (ACM)

##### Table of Contents  

<!--ts-->
* [Introduction](#introduction)
* [Anthos Config Management](#anthos-config-management-concept)
* [Configuration](#configuration)
* [Demos](#demos)
<!--te-->


Introduction
==========

So far, operation teams used to issue commands like the `kubectl` command to apply changes into their environments, which corresponds to an imperative way to operate. \
The result is that clusters start to grow, not only in number but in scale and size. We see a lot of large multi deployments by teams, which
can make harder by the respective teams to continue to manage these resources. \
So the solution for managing the cluster and resources passes on adopting a more declarative paradigm instead of an imperative one.
In other words, instead of issuing `kubectl` commands in the terminal in an attempt to change the state of our environments, we declare the state of the cluster and push that configuration, and that's where Anthos Config Management comes in.

ACM Concept
=============================

To make this transformation we need to create a central git repository, where we can store that configuration information about our cluster.
Such information regarding the namespaces, pods, services, secrets, volumes, metadata. \
The main idea relies on having a repository whose state matches exactly the state of the cluster.
Config management is really all about how do you store, control, and govern your deployment configuration.

More information: https://cloud.google.com/anthos-config-management/docs/overview

##### 2.1 Benefits of Software development process

Since we are dealing with a git repository, this approach inherits all the benefits with working with a version control software, which means the process of deploying a new config follows the same software development process.
I.e.: if a team needs to create a new configuration, they can branch the repository, they can make their changes, they can validate them locally, and then they can have them merge back in. And then those changes get pushed out to production. \
It becomes easier to audit changes. Instead of digging through Kubernetes logs and events and trying to figure out who made an erratic change, we can simply see the author of that commit. \
Additionally we can rollback to a more stable commit where our infrastructure was working properly. We can have scenarios where we can version and tag your deployments just like we version software.

More information: https://cloud.google.com/solutions/safe-rollouts-with-anthos-config-management

##### 2.2 Self Healing

ACM watches the cluster for drift from the configuration on the repo. \
If by chance someone changes the structure of our infrastructure, for instance renaming a specific namespace, the mechanism build into ACM will actually sink that change back in and put it back to the state reflected in the repository.

##### 2.3 ACM Policy Controller

Policy control is an important part of ACM and it consist on a set of rules or governance we want to apply into all environments or deployments. \
Policy controls **block** and **audit** actions that change the state of a specific object accourding to their specification. \
These rules are written by the people who know the compliance, the regulatory work, the organization's policies best. We don't want anything in production that violates these rules.

A policy is composed by two components:
- **Constraint Templates** - rule definition.
- **Constraints** - enforcing rules, which are instatiations of constraint templates and can be scoped to any particular namespace.

Example of policy use cases are:
- All pods and services deployed in a certain cluster have to specify the CPU and RAM they need for the job.
- Only allow the creation of resources in a cluster that respect a pre-defined name format, for instance the format: 
`<resource type>-<resource utility>-<resource-code>`.
- Only allow domain-restricted Roles and RoleBindings (certain users with certain actions).
- Require strict mTLS for all clients/services in a namespace.
- Require access logging to be enabled for a cluster /mesh.
- Require service-to-service authorized controls.

More information: https://cloud.google.com/anthos-config-management/docs/concepts/policy-controller

Configuration
=============

The ACM configuration relies on effectively telling the repository location, including branch and tag and give any authentication information for ACM to access the git repository.

##### 3.1 Enable Config Management
```
- Navigate to (on top left) Navigation Menu > Anthos > Config Management
- Click on Setup Button
- Click on Enable Config Management Button
```
##### 3.2 Setup Config Management
```
- Select the registered clusters to be managed by Anthos
- Click on Configure Button
- Choose SSH for Authentication type
- Generate a public/private rsa key locally
```
##### 3.3 Add public key on the repository
```
- Go to  https://gitlab.com/-/profile/keys
- Add the generated public key in the key section
- Click on Add key button
```
##### 3.4 Add the private key on cluster
```
- Issue the command from the gcp point 3). 
  This will create the namespace config-management-system and adding the private key to the cluster
```
##### 3.5 ACM settings for your clusters
```
- Leave version 1.7.0 in the field version
```
##### 3.6 Config sync.
Now we need to create a git repository that will store the config files of config management 
```    
- Enable Config sync.
- Copy the ssh url from the clone SSH button of the respective repository
- Leave Source format as hierarchy
```
##### 3.7 ACM Policy Controller Setup
Policy controller allows us to create rules that constraint the form we create and name resources in our selected clusters.
For instance we can create a policy that force the naming of certain resources to take certain format.
This is particularly good if we want to typify names allowing a better understanding of the cluster structure.
```
- Enable "Policy controller" checkbox
- Enable "Install default template library" checkbox
- Enable "Enable Constraint Templates..." checkbox
- Click in "Done" Button
```

##### 3.8 Creating the repository structure

```
- Perform a git clone on the project created before.
- Enter in the project and run `nomos init`.
- This will create the structure represented below.
- Submit the changes to the remote repository.
- In order to assure everything is properly configured, check in Anthos Config Management page if Config sync status and Policy controller status are Synced and Installed respectively.
```

| directories | meaning |
| ------ | ------ |
| namespaces/ | It contains configs for namespaces and namespace-scoped objects. |
| cluster/ | It contains configs that apply to entire clusters, rather than to Namespaces |
| clusterregistry/ | This directory is optional, and contains configs for ClusterSelectors |
| system/ | It contains configs for the Operator. repo.yaml file is automatically generated with nomos init command |


More information about a structured git repo.: https://cloud.google.com/kubernetes-engine/docs/add-on/config-sync/concepts/repo#structure_of_the_repo \
About Config Management: https://cloud.google.com/anthos-config-management/docs, \
About nomos command: https://cloud.google.com/kubernetes-engine/docs/add-on/config-sync/how-to/nomos-command

Demos
======

At this point the Anthos Config Management is properly configured, thus we can do different experiments for a better overall understading of this technology.

##### 4.1 Creating a namespace

Here the main goal is to create a new namespace through ACM.
Inside `namespace/` we created a folder with the same name of the desired namespace. \
Inside that folder we create the respective .yaml file named namespace.yaml containing the specification of the given namespace.
The content of that file is represented above:

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: test-ns
```
By pushing this file into the repository, Anthos will acknoledge this modification and will create that namespace on the selected clusters. \
We can find the new namespace by printing all the namespaces, using the command:
`kubectl get namespaces`

##### 4.2 Users & actions controll on the cluster
    
In this case, we try to declared a set of actions (or responsabilities) over a certain resource type and bind it to a group of users/ to a user. \
Firstly, we create a **cluster role** file, where it's specified the set of accepted actions and the type of resource.
In this case we selected the get, watch and list actions and the namespaces resources.

```yaml
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: namespace-reader
rules:
- apiGroups: [""]
  resources: ["namespaces"]
  verbs: ["get", "watch", "list"]
```
Secondly, we create a file usually named by **role binding**, where we define the group of people that can perform the actions described in the previous cluster role. \

```yaml
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: namespace-readers
subjects:
- kind: User
  name: johndoe@accenture.com
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: namespace-reader
  apiGroup: rbac.authorization.k8s.io
```

Both files must be placed inside `cluster/`. 
Again the files must be pushed to the repository for Anthos make the necessary changes.


Recognize the modification performed by typing this type of command:
`kubectl <action> --as=<user>`, for instance: `kubectl get namespaces --as=johndoe@accenture.com` \
We can also use this commando to receive a yes/no answer:
`kubectl auth can-i get namespaces --as=johndoe@accenture.com`

More information: [Cluster Role and Role Binding](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) \

##### 4.3 Creating a constraint #1

For creating a constraint we need to have Policy controller status installed on Anthos > Config Management. \
We can see the templates installed in the cluster by issuing:
`kubectl get constrainttemplates`
If the constraint template used on our constraint is not on te list, then we've to install it. \
`kubectl apply -f <template constraint yaml file>`

In our case we needed to install the K8sDenyName template, which is specified above:

```yaml
apiVersion: templates.gatekeeper.sh/v1beta1
kind: ConstraintTemplate
metadata:
  name: k8sdenyname
spec:
  crd:
    spec:
      names:
        kind: K8sDenyName
      validation:
        # Schema for the `parameters` field
        openAPIV3Schema:
          properties:
            invalidName:
              type: string
  targets:
    - target: admission.k8s.gatekeeper.sh
      rego: |
        package k8sdenynames
        violation[{"msg": msg}] {
          input.review.object.metadata.name == input.parameters.invalidName
          msg := sprintf("The name %v is not allowed", [input.parameters.invalidName])
        }
```

Then we proceed to place the constraint on a yaml file on the `cluster/` directory and push it to the repository.
The content of this yaml file is represented above:

```yaml
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sDenyName
metadata:
  name: no-policy-violation
spec:
  parameters:
    invalidName: "policy-violation"
```

In our case we created a constraint template that rejected the creation of any resource with a certain name string.
The forbidden name was declared on the constraint itself. \
In order to verify the modification performed we tried to create a namespace resource using the forbidden name.
The system instantly rejected the creation of such namespace thanks to the constraint. \


More information:
[Creating a constraint](https://cloud.google.com/anthos-config-management/docs/how-to/creating-constraints) \ 
[Writing a constraint](https://cloud.google.com/anthos-config-management/docs/how-to/write-a-constraint-template) \

##### 4.4 Creating a constraint #2

In this experiment we will create another constraint in `cluster/` folder.
The constraint consists in blocking the creation of all namespaces which not contain a "geo" label.
The constraint template used is the `K8sRequiredLabels` and it is already present in the list of constraint templates.
The content of the constraint is:

```yaml
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sRequiredLabels
metadata:
  name: ns-must-have-geo
spec:
  match:
    kinds:
      - apiGroups: [""]
        kinds: ["Namespace"]
  parameters:
    labels:
```

For testing the effect of this constraint, we create a .yaml file containing the specification of a namespace without the "geo" label , and execute the command: `kubectl create -f <yaml_namespace_file>`. The command action is going to be blocked.

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ns-with-no-label-geo
```


##### 4.4 Creating constraint for auditing

In this case, we will make use of constraints for auditing actions instead of blocking them.
So taking as example the constraint in 4.2., by adding the key value `enforcementAction: dryrun` on `spec`, whenever this constraint catches a bad action it won't block it but register. In this case a bad action is any action that creates a resource with the name "policy-violation".
In case of omission the `enforcementAction` field is set to `deny` value, that's why we don't need to specify this part when we want the constraint to block.


The constraint yaml file is represented above: 

```yaml
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sDenyName
metadata:
  name: audit-constraint
spec:
  enforcementAction: dryrun
  parameters:
    invalidName: "policy-violation"
```

After that we can try to create resources (for instance several "policy-violation" secrets over different namespaces). The system will accept them.
Afterwards we can retrieve information like the number of bad actions performed and specific details. We can do that by issuing the command:
`kubectl get constraint-kind constraint-name -o yaml`, which in our case is `kubectl get K8sDenyName audit-constraint -o yaml`

More information in: [Auditing Constraints](https://cloud.google.com/anthos-config-management/docs/how-to/auditing-constraints) \
[A simple git ACM repo](https://github.com/GoogleCloudPlatform/csp-config-management/tree/1.0.0/foo-corp) \
